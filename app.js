const Koa = require("koa");
const compose = require("koa-compose");

const MD = require("./src/middlewares/");
const utils = require("./src/utils");
const service = require("./src/service");

const app = new Koa();

const port = 8002;
const host = "0.0.0.0";

app.context.utils = utils;

app.use(compose(MD));

// 错误处理
app.on("error", (err, ctx) => {
  if (ctx) {
    ctx.body = {
      code: 500,
      message: `服务器内部错误：${err.message}`,
    };
  } else {
    ctx.body = {
      code: 500,
      message: `服务器内部顶级错误!`,
    };
  }
});

app.listen(port, host, () => {
  console.log(`API START ${host}:${port}`);
});
// const Koa = require("koa");
// const bodyParser = require("koa-bodyparser");

// const app = new Koa();

// // 使用第三方库帮助解析 json和urlencoded
// app.use(bodyParser());

// app.use((ctx, next) => {
//   console.log("中间件被执行");
//   console.log("ctx.request.body: ", ctx.request.body);

//   ctx.response.body = "Hello World!";
//   next();
// });

// app.listen(8002, () => {
//   console.log("server start");
// });
