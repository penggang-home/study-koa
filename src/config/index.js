const path = require("path");

const filePath = {
  //  临时文件存放地址
  tempFilePath: path.resolve(__dirname, "../../uploads"),
};

const LogSettings = {
  flag: true,
  // `${pwd}/src/log`
  outDir: path.resolve(__dirname, "../log"),
  // level: "info",
  level: "all",
};

const cloudSettings = {
  // 云环境ID
  env: "diancan-8gwgm6pp43236116",
  appid: "wxc70d5e8c68eb4564",
  // API baseURL
  baseURL: "https://api.weixin.qq.com/",
  // 小程序密钥
  secret: "2671927fa49747c7f4f493eebd0fa116",
};

module.exports = Object.assign(filePath, LogSettings, cloudSettings);
