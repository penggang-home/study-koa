const { add } = require("./add");
const { query } = require("./query");

module.exports = {
  add,
  query,
};
