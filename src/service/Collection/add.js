// 新增集合
const add = async (ctx, query) => {
  const { data } = await ctx.utils.$axios.post({
    url: "/tcb/databaseadd",
    data: {
      query,
    },
  });
  return data;
};

module.exports = {
  add,
};
