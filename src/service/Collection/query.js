// 查询记录
const query = async (ctx, query) => {
  const { data } = await ctx.utils.$axios.post({
    url: "/tcb/databasequery",
    data: {
      query,
    },
  });
  return data;
};

module.exports = {
  query,
};
