const { COS } = require("../utils");

// 公共API
const uploadController = async (ctx, next) => {
  // multer.any 方法,数组放在 ctx.request.files 和 ctx.files上，都有 一模一样 [{...}]

  // 需要上传到 腾讯云的 对象数组
  const uploads = {
    folderNames: [],
    fileNames: [],
    filePaths: [],
  };

  ctx.files.forEach((item) => {
    const names = item.originalname.split(".");
    uploads.folderNames.push(names[1]);
    uploads.fileNames.push(item.filename);
    uploads.filePaths.push(item.path);
  });

  console.log("uploads: ", uploads);
  // 需要上传的文件对象,上传完毕后是否删除本地文件
  const uploadResult = await COS.putObject(uploads, false);

  if (ctx.files.length == 0) {
    ctx.utils.thorwError(401, "文件上传失败");
  } else {
    ctx.body = {
      msg: `文件上传成功,共上传${ctx.files.length}个文件`,
      data: uploadResult,
    };
  }
};

module.exports = {
  uploadController,
};
