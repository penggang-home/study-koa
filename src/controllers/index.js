// 业务处理统一导出

const test = require("./test");
const login = require("./login");
const all = require("./all");

module.exports = {
  test,
  login,
  all,
};
