const list = async (ctx) => {
  // console.log("ctx.request.body: ", ctx.request.body);

  // 1.测试成功返回
  // ctx.body = ctx.request.body;
  // ctx.body = "返回结果";

  // 2.测试手动抛出错误
  // const data = "";
  // ctx.utils.assert(data, ctx.utils.throwError(10001, "验证码失效"));
  // ctx.body = "返回结果";

  // 3.测试服务器500
  // const b = a;
  // ctx.body = "返回结果";
  // console.log("ctx.request.params: ", ctx.request.query);
  // ctx.body = ctx.request.query;

  // 4.测试请求参数必传
  // const { name, age } = ctx.request.query;
  // ctx.body = name + age;

  // 5. 测试获取token
  // ctx.body = ctx.token;
  // ctx.utils.$axios.get({ url: "https://sdfsdf.dev/5w" });

  // 6.测试封装 axios
  // ctx.utils.$axios.post({ url: "/post", data: { age: 18 } }).then((res) => {
  //   console.log("res: ------------------------", res);
  // });

  // 7. 测试云开始

  // POST https://api.weixin.qq.com/tcb/databaseadd?access_token=ACCESS_TOKEN
  // let query = `db.collection("test").add({
  //   data:{name:"www.bookbook.cc"},
  // })`;
  // const data = await ctx.utils.$axios.post({
  //   url: "/tcb/databaseadd",
  //   data: {
  //     query,
  //   },
  // });

  // 8. 测试 token
  const token = ctx.request.header.authorization;
  try {
    const userInfo = ctx.verify(token.replace("Bearer ", ""));
    // console.log("userInfo: ", userInfo);
    ctx.body = userInfo;
  } catch (error) {
    // https://www.npmjs.com/package/jsonwebtoken Errors & Codes
    if (error.name === "TokenExpiredError") {
      ctx.utils.throwError(401, "登录状态已过期,请重新登录!");
    } else {
      ctx.utils.throwError(401, "未授权");
    }
  }
};

module.exports = {
  list,
};
