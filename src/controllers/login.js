/**
 * 登录注册接口
 */

const { Collection } = require("../service");

// 注册用户
const register = async (ctx, next) => {
  // 1.获取前端传递过来的值
  const { username, password } = ctx.request.body;

  //2. 查询是否已存在
  let queryExist = `db.collection('business-acc').where({username:'${username}'}).get()`;
  const { data: ExistResult } = await Collection.query(ctx, queryExist);

  if (ExistResult.length > 0) {
    // 用户已存在
    console.log("用户已存在");
    ctx.utils.throwError(400, "此用户已存在!");
  } else {
    // 1.准备数据
    // uid转化为字符串
    const uid = JSON.stringify(Date.now());
    let dbData = {
      username,
      password,
      uid,
    };
    dbData = JSON.stringify(dbData);

    // 2.准备SQL语句
    let query = `db.collection("business-acc").add({data:${dbData}})`;
    const result = await Collection.add(ctx, query);

    if (result.errcode === 0) {
      console.log("新增成功");
      ctx.body = result;
    } else {
      console.log("新增失败：");
      ctx.utils.throwError(400, "新增失败：" + result.errmsg);
    }
  }
};

// 登录
const login = async (ctx, next) => {
  // 1.获取前端传递过来的值
  const { username, password } = ctx.request.body;

  //2. 查询是否已存在
  let queryExist = `db.collection('business-acc').where({username:'${username}'}).get()`;
  let { data } = await Collection.query(ctx, queryExist);
  if (data.length === 0) {
    console.log("用户不存在！: ");
    ctx.utils.throwError(401, "用户不存在！");
  } else {
    data = JSON.parse(data);
    if (username === data.username && password === data.password) {
      // 登录成功
      const token = ctx.sign(data);
      ctx.body = { token, ...data };
      // ctx.body = data;
    } else {
      ctx.utils.throwError(401, "密码错误!");
    }
  }
};
module.exports = {
  register,
  login,
};
