// 路由列表文件
const { test, login, all } = require("../controllers");
const { scmTest, validLogin } = require("../validate");

// 全局公共接口中间件
const { upload } = require("../utils");

// 公共接口
const AllRoute = [
  {
    method: "post",
    path: "/upload",
    controller: [upload.any(), all.uploadController],
  },
];

// 登录路由
const loginRoute = [
  {
    method: "post",
    path: "/register",
    valid: validLogin.register,
    controller: login.register,
  },
  {
    method: "post",
    path: "/login",
    valid: validLogin.login,
    controller: login.login,
  },
];

const routes = [
  {
    //  测试
    method: "post",
    path: "/a",
    valid: scmTest.list,
    controller: test.list,
  },
  ...AllRoute,
  ...loginRoute,
];

module.exports = routes;
