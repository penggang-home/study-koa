const koaRouter = require("koa-router");
const router = new koaRouter({ prefix: "/api" });

const routeList = require("./routes");
// 参数校验
const paramValidator = require("../middlewares/paramValidator");

routeList.forEach((item) => {
  const { method, path, controller, valid } = item;
  if (controller instanceof Array) {
    router[method](path, paramValidator(valid), ...controller);
  } else {
    router[method](path, paramValidator(valid), controller);
  }
});

module.exports = router;
