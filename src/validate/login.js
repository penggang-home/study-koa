const Joi = require("@hapi/joi");

const register = {
  body: Joi.object({
    username: Joi.string()
      .pattern(/^\d{8,20}$/)
      .required()
      .error(new Error("用户名格式错误：由8-20位数字组成")),
    password: Joi.string()
      .required()
      .alphanum()
      .min(6)
      .max(18)
      .error(new Error("密码格式错误：由6-18位[a-zA-Z0-9]字符串组成")),
  }),
};
const login = {
  body: Joi.object({
    username: Joi.string()
      .pattern(/^\d{8,20}$/)
      .required()
      .error(new Error("用户名格式错误：由8-20位数字组成")),
    password: Joi.string()
      .required()
      .alphanum()
      .min(6)
      .max(18)
      .error(new Error("密码格式错误：由6-18位[a-zA-Z0-9]字符串组成")),
  }),
};

module.exports = {
  register,
  login,
};
