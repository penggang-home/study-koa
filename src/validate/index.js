const scmTest = require("./test");
const validLogin = require("./login");

module.exports = {
  scmTest,
  validLogin,
};
