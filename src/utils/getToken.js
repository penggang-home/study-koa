const fs = require("fs");
const path = require("path");

const axios = require("axios");

const config = require("../config");

// 获取token的地址
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/access-token/auth.getAccessToken.html

let url = `https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=${config.appid}&secret=${config.secret}`;

async function getToken() {
  const { data } = await axios.request({ url });
  let token = data.access_token;
  const tokenInfo = {
    token,
    timeStamp: Date.now(),
    DateTime: new Date().toLocaleString(),
  };
  fs.writeFileSync(
    path.resolve(__dirname, "./token.json"),
    JSON.stringify(tokenInfo),
    {
      flag: "w+",
    }
  );
  return token;
}
// 读取token,判断有误 没有或者过期直接重新请求
async function init() {
  let token = fs.readFileSync(path.resolve(__dirname, "./token.json"), {
    encoding: "utf-8",
  });
  token = JSON.parse(token);
  if (!token || token.timeStamp / 1000 + 7200 < Date.now() / 1000) {
    await getToken();
  } else {
    return token.token;
  }
}
init();

setInterval(async () => {
  getToken();
  // token 有效期 7200s 2小时
}, 7000 * 1000);

module.exports = () => {
  return init();
};
