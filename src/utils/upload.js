// 文件上传配置
const fs = require("fs");
const path = require("path");
const multer = require("@koa/multer");

const { tempFilePath } = require("../config");

const storage = multer.diskStorage({
  // 文件存放位置
  destination: function (req, file, cb) {
    // 拿到文件名称 和 扩展名
    const names = file.originalname.split(".");
    const realFilePath = path.join(tempFilePath, `/${names[1]}`);

    try {
      // 判断文件是否存在，不存在抛出错误
      fs.statSync(realFilePath);
    } catch (error) {
      // 不存在则创建文件
      fs.mkdirSync(realFilePath);
    }

    cb(null, realFilePath);
  },
  // 文件名称
  filename: function (req, file, cb) {
    const names = file.originalname.split(".");
    cb(null, names[0] + "-" + Date.now() + "." + names[1]);
  },
});
const upload = multer({ storage });

module.exports = upload;
