const axios = require("axios");
const ApplicationConfig = require("../config");

const getToken = require("./getToken.js");

// 封装 axios
class Request {
  // 1.创建axios的实例
  constructor(config) {
    this.instance = axios.create({
      baseURL: "https://api.weixin.qq.com/",
      timeout: 10000,
      ...config,
    });

    // 添加请求拦截器
    this.instance.interceptors.request.use(
      async function (config) {
        // 获取Token
        const token = await getToken();
        // 添加Token
        config.url += `?access_token=${token}`;
        // 添加环境ID
        config.data.env = ApplicationConfig.env;
        return config;
      },
      function (error) {
        // 对请求错误做些什么
        return Promise.reject(error);
      }
    );
  }
  request(config) {
    return this.instance.request(config);
  }
  get(config) {
    config.method = "GET";
    return this.instance.request({ ...config });
  }
  post(config) {
    config.method = "POST";
    return this.instance.request({ ...config });
  }
}

const $axios = new Request();

module.exports = {
  $axios,
};
