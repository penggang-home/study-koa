const assert = require("assert");

const { $axios } = require("./request");
const upload = require("./upload");
const COS = require("./COS");

const throwError = (code, message) => {
  const err = new Error(message);
  err.code = code;
  throw err;
};

module.exports = {
  $axios,
  assert,
  throwError,
  upload,
  COS,
};
