const fs = require("fs");

const COS = require("cos-nodejs-sdk-v5");

const cos = new COS({
  SecretId: "AKIDqVu2tUorcEZdQxEExOZS4PVKs9AvPcYD",
  SecretKey: "QZQ1LN9drnSqxnMr52i7F6gz0lFSO2RZ",
});

// 1.查询存储桶列表
// cos.getService(function (err, data) {
//   console.log(err || data);
// });

// 2.创建存储桶
// cos.putBucket(
//   {
//     Bucket: "uploads-1302314564",
//     Region: "ap-chengdu",
//     ACL: "public-read", // 权限
//   },
//   function (err, data) {
//     console.log(err || data);
//   }
// );

// 3. 上传文件
const putObject = (uploads, flag = false) => {
  return new Promise((resolve, reject) => {
    // 上传完毕的数组
    const uploadsArr = [];
    const { folderNames, fileNames, filePaths } = uploads;
    console.log("filePaths: ", filePaths);

    folderNames.forEach((item, i) => {
      // 文件夹/文件名
      const Key = folderNames[i] + "/" + fileNames[i];

      // 上传配置
      const cosSettings = {
        Bucket: "uploads-1302314564" /* 存储桶名称 必须 */,
        Region: "ap-chengdu" /* 存储桶地域 必须 */,
        StorageClass: "STANDARD", //存储类型： 标准存储 低频存储 等
        Key: Key /* 必须 文件名称 */,
        Body: fs.createReadStream(filePaths[i]), // 上传文件对象
        ContentLength: fs.statSync(filePaths[i]).size, // 当Body为文件流时，必须传文件大小，否则onProgress不能返回正确的进度信息
        onProgress: function (progressData) {
          console.log("onProgress:", JSON.stringify(progressData));
        },
      };

      cos.putObject(cosSettings, (err, data) => {
        // 1.保留上传数据
        if (data) {
          uploadsArr.push({
            fileName: fileNames[i],
            url: "https://" + data.Location,
          });
        } else {
          console.log("uploads err: ", err);
          uploadsArr.push({
            fileName: fileNames[i],
            url: "上传失败~",
          });
        }
        // 2.删除本地文件
        if (flag) {
          fs.rm(filePaths[i], (err) => {
            if (err) console.log("删除本地文件失败：", err);
          });
        }
        // 3.上传完成，返回上传的数据
        if (uploadsArr.length == uploads.fileNames.length) {
          console.log("上传完成~");
          resolve(uploadsArr);
        }
      });
    });
  });
};

module.exports = {
  putObject,
};
