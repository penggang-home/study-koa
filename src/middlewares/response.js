const response = () => {
  return async (ctx, next) => {
    ctx.res.fail = ({ code, data, msg }) => {
      ctx.body = {
        code: code || 400,
        data: data || {},
        msg: msg || "Error(Default)",
      };
      ctx.status = code || 400;
    };

    ctx.res.success = (msg) => {
      ctx.body = {
        code: 200,
        data: ctx.body,
        msg: msg || "success",
      };
    };

    await next();
  };
};

module.exports = response;
