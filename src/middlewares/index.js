/**
 * 引入第三方插件
 */
const koaBody = require("koa-bodyparser");
const cors = require("@koa/cors");

/**
 * 引入自定义文件
 */
const router = require("../router");
const response = require("./response");
const error = require("./error");
const log = require("./log");

/**
 * 记录请求日志
 */
const mdLogger = log();

/**
 * 参数解析
 * https://github.com/koajs/bodyparser
 */
const mdKoaBody = koaBody({
  enableTypes: ["json", "form", "text", "xml"],
  formLimit: "56kb",
  jsonLimit: "1mb",
  textLimit: "1mb",
  xmlLimit: "1mb",
  strict: true,
});

/**
 * 统一返回格式 与 错误处理
 */
const mdResHandler = response();
const mdErrorHandler = error();

/**
 * 跨域处理
 */
const mdCors = cors({
  origin: "*",
  credentials: true,
  allowMethods: ["GET", "HEAD", "PUT", "POST", "DELETE", "PATCH"],
});

/**
 * 路由处理
 */
const mdRoute = router.routes();
/*
    原先当路由存在，请求方式不匹配的时候(期望POST却是GET)，会报 404，
    加了这个中间件，会报请求方式不被允许 Method Not Allowed
*/
const mdRouterAllowed = router.allowedMethods();

const jwt = require("./jwt");
module.exports = [
  mdResHandler,
  mdErrorHandler,
  mdCors,
  mdKoaBody,
  mdLogger,
  jwt(),
  mdRoute,
  mdRouterAllowed,
];
