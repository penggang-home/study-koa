const fs = require("fs");
const path = require("path");
const jwt = require("jsonwebtoken");

const PRIVATE_KEY = fs.readFileSync(
  path.resolve(__dirname, "../config/keys/private.key"),
  { encoding: "utf-8" }
);
const PUBLIC_KEY = fs.readFileSync(
  path.resolve(__dirname, "../config/keys/public.key"),
  { encoding: "utf-8" }
);

// 派发 token
const sign = (data, uid = "") => {
  const token = jwt.sign({ ...data, uid }, PRIVATE_KEY, {
    expiresIn: 7200, //过期时间 单位：s
    // 指定加密算法 RS256=>非对称加密(安全,速度不及HS256) HS256=>对称加密(不及RS256安全,但速度快)默认
    algorithm: "RS256",
  });
  return token;
};
// 验证 token
const verify = (token) => {
  const result = jwt.verify(token, PUBLIC_KEY, {
    // 指定了什么加密，则用什么解密，可以指定多个解密算法
    algorithms: ["RS256"],
  });
  return result;
  // 验证不通过或失败会抛出异常
  // ctx.body = `验证结果：${JSON.stringify(result)}`;
};

// let str = `eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiIyZDQ0ZDZjMjYxNjA3MzQ1MTFlZjc2YzA3NjhkZWZhOSIsInBhc3N3b3JkIjoiMTU4ODQ3ODE0NzciLCJ1aWQiOiIiLCJ1c2VybmFtZSI6IjE1ODg0NzgxNDc3IiwiaWF0IjoxNjMzNzg2NDY4LCJleHAiOjE2MzM3OTM2Njh9.D9tBHt0-RJE9r3_LBlokVHrEKDwia1NVOdXvOajOOiqJEVT8Jf4jBFbBOU786g4eFhI8RVMSLwpbuycNtqej9V2qvbeOLDw0YcG9-ZZCzfEfkOAa8EXqgIhg06gQ9t9d7RMhus_fQSKgk1s5_8WAs6ega-VCFpvV5RqC7J1s-oDgBx7lAT271IhUo6njp2UmDR6sij0bBivUwbhlyfNyyha32nlHpjIWC8m-z1nimclpr1mRBk5r7XdHDYZLWTkLEdwzxpwQHURsgemRhagDFNbP_i6YDKxGdCTGOCIbvKL_FkhAI7sC1YclBBWhV24MJTVQgXVBxXgf8xUdG6w4Ew`;

// console.log(verify(str));
module.exports = () => {
  return async (ctx, next) => {
    ctx.sign = sign;
    ctx.verify = verify;
    await next();
  };
};
